import React, { FC } from "react";
import Image from 'next/image';

import style from "../../../styles/Card.module.scss";

export interface ICard {
    img: string,
    title: string,
    subtitle?: string
    description: string,
    sm?: boolean
}

const Card: FC<ICard> = (props) => {
    const smallClass = props.sm ? style.sm : ""
    return (
        <div className={`${style.card} ${smallClass}`}>
            <div className={style.avatar}>
                <Image src={props.img} width={115} height={115} />
                <div>
                    <h2>{ props.title }</h2>
                    <p>{ props.subtitle }</p>
                </div>
            </div>
            <div className={style.wrapper}>
                <p className={style.description}>
                    { props.description }
                </p>
            </div>
        </div>
    );
};

export default Card;
