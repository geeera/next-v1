import { FC } from "react";
import Image from "next/image";
import styles from "../../../styles/Coder.module.scss";

export interface ICoder {
    title: string,
    fullName: string,
    avatarUri: string,
    rate: number,
    time: string
}

const stars = [0, 1, 2, 3, 4]

const Coder: FC<ICoder> = (props) => {
    return (
        <div className={styles.coder}>
            <div>
                <Image src={props.avatarUri} width={50} height={50} />
                <div>
                    <h3>{props.title}</h3>
                    <span>{props.fullName}</span>
                </div>
            </div>
            <div>
                <span>{props.time}</span>
                <div>
                    {stars.map((_, idx) => {
                        const activeClass = idx + 1 <= props.rate ? styles.active : ''
                        return <span
                            key={Math.random() * 1000}
                            className={`${styles.star} ${activeClass}`}
                        />
                    })}
                </div>
            </div>
        </div>
    );
};

export default Coder;
