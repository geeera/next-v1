import style from "../../../styles/StatisticCard.module.scss";
import { useEffect, useState } from "react";

const apiUri = "https://api.coingecko.com/api/v3/";

type ApiResType = {
    ath: number
    ath_change_percentage: number
    ath_date: string
    atl: number
    atl_change_percentage: number
    atl_date: string
    circulating_supply: number
    current_price: number
    fully_diluted_valuation: number
    high_24h: number
    id: string
    image: string
    last_updated: string
    low_24h: number
    market_cap: number
    market_cap_change_24h: number
    market_cap_change_percentage_24h: number
    market_cap_rank: number
    max_supply: number
    name: string
    price_change_24h: number
    price_change_percentage_24h: number
    roi: null
    symbol: string
    total_supply: number
    total_volume: number
}

function StatisticCard() {
    const [coins, setCoins] = useState<ApiResType[]>([])
    const fetchData = async () => {
        const res = await fetch(apiUri + "coins/markets?vs_currency=usd");
        return await res.json();
    }


    useEffect(() => {
        const result = fetchData();
        result.then(res => {
            setCoins(res)
        })
    }, [])
    const [first, second] = coins;

    return (
        <div className={`${style.card} ${style.row}`}>
            <div className={style.col}>
                <h3 className={style.title}>Fonts & Colors <br/>
                    Matter</h3>
                <div className={style.widget}>
                    <h4>{first?.name}</h4>
                    <h2>${first?.current_price} <span
                        className={first?.price_change_percentage_24h > 0
                            ? style.up
                            : style.down}
                    >{first?.price_change_percentage_24h.toFixed(2)}%</span></h2>
                    <p>You’ll Learn A Lot From This Task</p>
                </div>
            </div>
            <div className={style.col}>
                <div className={style.widget}>
                    <h4>{second?.name}</h4>
                    <h2>${second?.current_price} <span
                        className={second?.price_change_percentage_24h > 0
                            ? style.up
                            : style.down}
                    >{first?.price_change_percentage_24h.toFixed(2)}%</span></h2>
                    <p>You’ll Learn A Lot From This Task</p>
                </div>
            </div>
        </div>
    );
};

export default StatisticCard;
