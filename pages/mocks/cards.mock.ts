import { ICard } from "../components/Card/Card";

export const cards: ICard[] = [
    {
        img: "/avatars/nia.png",
        title: "Nia",
        description: "“This task can be perfect for students from FIT”"
    },
    {
        img: "/avatars/abigale.png",
        title: "Abigale",
        description: "“I am so sexy, that men are doing  test tasks for me for free.”"
    },
    {
        img: "/avatars/fedya.png",
        title: "Fedya",
        description: "“Kuku Epta. EZ Task.”"
    },
]

export const card: ICard = {
    img: "/avatars/andy.png",
    title: "Andy",
    subtitle: "Head Of UX Design",
    description: "“The Style Of This Test Task\n" +
        "Matches The Style That You Will\n" +
        "Be Making Real Job In.”"
}
