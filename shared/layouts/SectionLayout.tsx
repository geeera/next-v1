import { FC, PropsWithChildren } from "react";

interface ISectionLayoutProps {
    title: string
}
const SectionLayout: FC<PropsWithChildren<ISectionLayoutProps>> = (props) => {
    return (
        <section className="layout">
            <header>
                <h4 className="section-title">{ props.title }</h4>
            </header>
            <main>
                { props.children }
            </main>
        </section>
    );
};

export default SectionLayout;
